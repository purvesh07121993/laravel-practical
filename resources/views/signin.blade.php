@extends('elements.login_master')
@section('content')



<div class="container-login100">
            <div class="wrap-login100">
                {!! Form::open(array('route' => 'login-check','method'=>'POST','files'=>'true','class'=>'login100-form validate-form')) !!}
                    <span class="login100-form-title p-b-43">
                        Login to continue
                    </span>

                    
                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="input100" type="text" name="email">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                    </div>
                    
                    
                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="pass">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Password</span>
                    </div>

                    

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>
                {!! Form::close() !!}

                <div class="login100-more" style="background-image: url('images/bg-01.jpg');">
                </div>
            </div>
        </div>
@endsection