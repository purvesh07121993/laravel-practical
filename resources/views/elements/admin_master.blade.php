<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
	<meta charset="utf-8" />
	<title>@yield('page-title')</title>
	<meta name="description" content="Latest updates and statistic charts"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<!--begin::Web font -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!--end::Web font -->
	<!--begin::Global Theme Styles -->
	<link href="{{ URL::asset('assets/css/bootstrap-datepickernew.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<link href="{{ URL::asset('assets/css/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles -->
	<!--begin::Page Vendors Styles -->
	<link href="{{ URL::asset('assets/plugin/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Page Vendors Styles -->
	<link rel="shortcut icon" href="{{ URL::asset('assets/default/media/img/logo/faviconi.png') }}" />
</head>
<!-- end::Head -->
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- BEGIN: Header -->
		<header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
			<div class="m-container m-container--fluid m-container--full-height">
				<div class="m-stack m-stack--ver m-stack--desktop">   
					<!-- BEGIN: Brand -->
					<div class="m-stack__item m-brand  m-brand--skin-dark ">
						<div class="m-stack m-stack--ver m-stack--general">
							<div class="m-stack__item m-stack__item--middle m-brand__logo">
								<a href="{{ route('admin-dashboard') }}" class="m-brand__logo-wrapper">
									<img alt="" src="{{ URL::asset('assets/images/logos/DBP2.png') }}" style="height: 20px;">
								</a>  
							</div>
							<div class="m-stack__item m-stack__item--middle m-brand__tools">
								<!-- BEGIN: Left Aside Minimize Toggle -->
								<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
									<span></span>
								</a>
								<!-- END -->
								<!-- BEGIN: Responsive Aside Left Menu Toggler -->
								<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
									<span></span>
								</a>
								<!-- END -->
								<!-- BEGIN: Responsive Header Menu Toggler -->
							<!-- <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
								<span></span>
							</a> -->
							<!-- END -->
							<!-- BEGIN: Topbar Toggler -->
							<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
								<i class="flaticon-more"></i>
							</a>
							<!-- BEGIN: Topbar Toggler -->
						</div>
					</div>
				</div>
				<!-- END: Brand -->     
				<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
					<!-- BEGIN: Topbar -->
					<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
						<div class="m-stack__item m-topbar__nav-wrapper">
							<ul class="m-topbar__nav m-nav m-nav--inline">    
								<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right  m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
									<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
										<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
										<span class="m-nav__link-icon"><i class="flaticon-alarm"></i></span>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__header m--align-center" style="background: url({{ URL::asset('assets/images/misc/notification_bg.jpg') }}); background-size: cover;">
												<span class="m-dropdown__header-title">9 New</span>
												<span class="m-dropdown__header-subtitle">Admin Notifications</span>
											</div>
											<div class="m-dropdown__body">        
												<div class="m-dropdown__content">
													<div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
														<div class="m-list-timeline m-list-timeline--skin-light">
															<div class="m-list-timeline__items">
																<div class="m-list-timeline__item">
																	<span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
																	<span class="m-list-timeline__text">12 new users registered</span>
																	<span class="m-list-timeline__time">Just now</span>
																</div>
																<div class="m-list-timeline__item">
																	<span class="m-list-timeline__badge"></span>
																	<span class="m-list-timeline__text">System shutdown <span class="m-badge m-badge--success m-badge--wide">pending</span></span>
																	<span class="m-list-timeline__time">14 mins</span>
																</div>
																<div class="m-list-timeline__item">
																	<span class="m-list-timeline__badge"></span>
																	<span class="m-list-timeline__text">New invoice received</span>
																	<span class="m-list-timeline__time">20 mins</span>
																</div>
																<div class="m-list-timeline__item">
																	<span class="m-list-timeline__badge"></span>
																	<span class="m-list-timeline__text">DB overloaded 80% <span class="m-badge m-badge--info m-badge--wide">settled</span></span>
																	<span class="m-list-timeline__time">1 hr</span>
																</div>
																<div class="m-list-timeline__item">
																	<span class="m-list-timeline__badge"></span>
																	<span class="m-list-timeline__text">System error - <a href="#" class="m-link">Check</a></span>
																	<span class="m-list-timeline__time">2 hrs</span>
																</div>
																<div class="m-list-timeline__item m-list-timeline__item--read">
																	<span class="m-list-timeline__badge"></span>
																	<span href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--danger m-badge--wide">urgent</span></span>
																	<span class="m-list-timeline__time">7 hrs</span>
																</div>
																<div class="m-list-timeline__item m-list-timeline__item--read">
																	<span class="m-list-timeline__badge"></span>
																	<span class="m-list-timeline__text">Production server down</span>
																	<span class="m-list-timeline__time">3 hrs</span>
																</div>
																<div class="m-list-timeline__item">
																	<span class="m-list-timeline__badge"></span>
																	<span class="m-list-timeline__text">Production server up</span>
																	<span class="m-list-timeline__time">5 hrs</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
									<a href="#" class="m-nav__link m-dropdown__toggle">
										<span class="m-topbar__userpic">  
											@if(Auth::user()->photo != "")
											<img src="{{ URL::asset('assets/uploads/user').'/'.Auth::user()->photo }}" class="m--img-rounded m--marginless" alt=""/>
											@else
											<img src="{{ URL::asset('assets/images/users/user4.jpg') }}" class="m--img-rounded m--marginless" alt=""/>
											@endif
										</span>
										<span class="m-topbar__username m--hide">{{ Auth::user()->full_name }}</span>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__header m--align-center" style="background: url({{ URL::asset('assets/images/misc/user_profile_bg.jpg') }}); background-size: cover;">
												<div class="m-card-user m-card-user--skin-dark">
													<div class="m-card-user__pic">
														@if(Auth::user()->photo != "")
														<img src="{{ URL::asset('assets/uploads/user').'/'.Auth::user()->photo }}" class="m--img-rounded m--marginless" alt=""/>
														@else
														<img src="{{ URL::asset('assets/images/users/user4.jpg') }}" class="m--img-rounded m--marginless" alt=""/>
														@endif
													</div>
													<div class="m-card-user__details">
														<span class="m-card-user__name m--font-weight-500">{{ Auth::user()->full_name }}</span>
													</div>
												</div>
											</div>
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav m-nav--skin-light">
														<li class="m-nav__section m--hide">
															<span class="m-nav__section-text">Section</span>
														</li>
														<li class="m-nav__item">
															<a href="{{ route('admin-profile') }}" class="m-nav__link">
																<i class="m-nav__link-icon la la-user"></i>
																<span class="m-nav__link-text">My Profile</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="{{ route('change-password') }}" class="m-nav__link">
																<i class="m-nav__link-icon la la-unlock-alt"></i>
																<span class="m-nav__link-text">Change Password</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit">
														</li>
														<li class="m-nav__item">
															<a href="javascript:" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																Logout
															</a>    
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- END: Topbar --> 
				</div>
			</div>
		</div>
	</header>
	<!-- END: Header -->  
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		<!-- BEGIN: Left Aside -->
		<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
		<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">  
			<!-- BEGIN: Aside Menu -->
			<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">    
				<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
					<li class="m-menu__item {{ request()->is('dashboard') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
						<a  href="{{ route('admin-dashboard') }}" class="m-menu__link ">
							<i class="m-menu__link-icon la la-dashboard"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Dashboard</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@if(Route::currentRouteName() == 'admin-user' || Route::currentRouteName() == 'admin-add' || Route::currentRouteName() == 'admin-update' || Route::currentRouteName() == 'admin-details')         
					@php $activeAdminData = "m-menu__item--active" @endphp
					@else
					@php $activeAdminData = "" @endphp
					@endif
					@if(in_array('admin-user',$accessData))
					<li class="m-menu__item {{ $activeAdminData }}" aria-haspopup="true">
						<a href="{{ route('admin-user') }}" class="m-menu__link ">
							<i class="m-menu__link-icon la la-users"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Admin User</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'customer-agent' || Route::currentRouteName() == 'customer-add' || Route::currentRouteName() == 'customer-update' || Route::currentRouteName() == 'customer-details')         
					@php $activeCustomerData = "m-menu__item--active" @endphp
					@else
					@php $activeCustomerData = "" @endphp
					@endif
					@if(in_array('customer-agent',$accessData))
					<li class="m-menu__item {{ $activeCustomerData }}" aria-haspopup="true">
						<a href="{{ route('customer-agent') }}" class="m-menu__link ">
							<i class="m-menu__link-icon la la-users"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Customer & Agent</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'customer-as-per-agent' || Route::currentRouteName() == 'customer-agent-details')         
					@php $activeCustomerAgentData = "m-menu__item--active" @endphp
					@else
					@php $activeCustomerAgentData = "" @endphp
					@endif
					@if(in_array('customer-as-per-agent',$accessData))
					<li class="m-menu__item {{ $activeCustomerAgentData }}" aria-haspopup="true">
						<a href="{{ route('customer-as-per-agent') }}" class="m-menu__link ">
							<i class="m-menu__link-icon la la-users"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Agent Customer</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'promo-code' || Route::currentRouteName() == 'promo-code-add' || Route::currentRouteName() == 'promo-code-save' || Route::currentRouteName() == 'promo-code-update' || Route::currentRouteName() == 'promo-code-save-update' || Route::currentRouteName() == 'promo-code-delete')         
					@php $activePromoCodeData = "m-menu__item--active" @endphp
					@else
					@php $activePromoCodeData = "" @endphp
					@endif
					@if(in_array('promo-code',$accessData))
					<li class="m-menu__item {{ $activePromoCodeData }}" aria-haspopup="true">
						<a href="{{ route('promo-code') }}" class="m-menu__link ">
							<i class="m-menu__link-icon fa fa-book"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Promo Code</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'state' || Route::currentRouteName() == 'state-add' || Route::currentRouteName() == 'state-save' || Route::currentRouteName() == 'state-update' || Route::currentRouteName() == 'state-save-update' || Route::currentRouteName() == 'state-delete')         
					@php $activePromoCodeData = "m-menu__item--active" @endphp
					@else
					@php $activePromoCodeData = "" @endphp
					@endif
					@if(in_array('state',$accessData))
					<li class="m-menu__item {{ $activePromoCodeData }}" aria-haspopup="true">
						<a href="{{ route('state') }}" class="m-menu__link ">
							<i class="m-menu__link-icon fa fa-book"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">State</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'save-quotes' || Route::currentRouteName() == 'save-quotes-detail')         
					@php $activeSaveQuote = "m-menu__item--active" @endphp
					@else
					@php $activeSaveQuote = "" @endphp
					@endif
					@if(in_array('promo-code',$accessData))
					<li class="m-menu__item {{ $activeSaveQuote }}" aria-haspopup="true">
						<a href="{{ route('save-quotes') }}" class="m-menu__link ">
							<i class="m-menu__link-icon fa fa-book"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Manage Saved Quotes</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'orders' || Route::currentRouteName() == 'orders-detail')         
					@php $activeOrder = "m-menu__item--active" @endphp
					@else
					@php $activeOrder = "" @endphp
					@endif
					@if(in_array('orders',$accessData))
					<li class="m-menu__item {{ $activeOrder }}" aria-haspopup="true">
						<a href="{{ route('orders') }}" class="m-menu__link ">
							<i class="m-menu__link-icon fa fa-book"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Manage Order</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'book-step' || Route::currentRouteName() == 'book-step-add' || Route::currentRouteName() == 'book-step-save' || Route::currentRouteName() == 'book-step-update' || Route::currentRouteName() == 'book-step-save-update' || Route::currentRouteName() == 'book-step-delete')         
					@php $activeBookStepData = "m-menu__item--active" @endphp
					@else
					@php $activeBookStepData = "" @endphp
					@endif
					@if(in_array('book-step',$accessData))
					<li class="m-menu__item {{ $activeBookStepData }}" aria-haspopup="true">
						<a href="{{ route('book-step') }}" class="m-menu__link ">
							<i class="m-menu__link-icon fa fa-book"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Book Step</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'book-step-options' || Route::currentRouteName() == 'book-step-selected' || Route::currentRouteName() == 'book-step-option-add' || Route::currentRouteName() == 'book-step-option-update' || Route::currentRouteName() == 'book-step-option-details')        
					@php $activeBookStepOptionData = "m-menu__item--active" @endphp
					@else
					@php $activeBookStepOptionData = "" @endphp
					@endif
					@if(in_array('book-step-options',$accessData))
					<li class="m-menu__item {{ $activeBookStepOptionData }}" aria-haspopup="true">
						<a href="{{ route('book-step-options') }}" class="m-menu__link ">
							<i class="m-menu__link-icon la la-server"></i>
							<span class="m-menu__link-title">  
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">Book Step Option</span>
									<span class="m-menu__link-badge"></span>
								</span>
							</span>
						</a>
					</li>
					@endif
					@if(Route::currentRouteName() == 'general-settings-role' || Route::currentRouteName() == 'general-settings-role-add' || Route::currentRouteName() == 'general-settings-role-update' || Route::currentRouteName() == 'general-settings-role-permission' || Route::currentRouteName() == 'general-settings-role-permission-add' || Route::currentRouteName() == 'general-settings-role-Permission-update' || Route::currentRouteName() == 'book-quantity' || Route::currentRouteName() == 'book-page-count' || Route::currentRouteName() == 'quote-durations' || Route::currentRouteName() == 'partial-payment-percentage')         
					@php $activeGeneralSettingsData = "m-menu__item--open m-menu__item--expanded" @endphp
					@else
					@php $activeGeneralSettingsData = "" @endphp
					@endif
					<li class="m-menu__item m-menu__item--submenu {{ $activeGeneralSettingsData }} " aria-haspopup="true" m-menu-submenu-toggle="hover">
						<a href="javascript:;" class="m-menu__link m-menu__toggle">
							<i class="m-menu__link-icon la la-cogs"></i>
							<span class="m-menu__link-text">General Settings</span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
						</a>
						<div class="m-menu__submenu " m-hidden-height="80" style="">
							<span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
									<span class="m-menu__link">
										<span class="m-menu__link-text">General Settings</span>
									</span>
								</li>
								@if(Route::currentRouteName() == 'general-settings-role' || Route::currentRouteName() == 'general-settings-role-add' || Route::currentRouteName() == 'general-settings-role-update')         
								@php $activeRoleData = "m-menu__item--active" @endphp
								@else
								@php $activeRoleData = "" @endphp
								@endif
								@if(in_array('general-settings-role',$accessData))
								<li class="m-menu__item {{ $activeRoleData }}" aria-haspopup="true">
									<a href="{{ Route('general-settings-role') }}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">Role</span>
									</a>
								</li>
								@endif
								@if(Route::currentRouteName() == 'general-settings-role-permission' || Route::currentRouteName() == 'general-settings-role-permission-add' || Route::currentRouteName() == 'general-settings-role-Permission-update')         
								@php $activeRolePermissionsData = "m-menu__item--active" @endphp
								@else
								@php $activeRolePermissionsData = "" @endphp
								@endif
								@if(in_array('general-settings-role-permission',$accessData))
								<li class="m-menu__item {{ $activeRolePermissionsData }}" aria-haspopup="true">
									<a href="{{ Route('general-settings-role-permission') }}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">Role Permissions</span>
									</a>
								</li>
								@endif
								@if(Route::currentRouteName() == 'book-quantity')         
								@php $activeBookQuantityData = "m-menu__item--active" @endphp
								@else
								@php $activeBookQuantityData = "" @endphp
								@endif
								@if(in_array('book-quantity',$accessData))
								<li class="m-menu__item {{ $activeBookQuantityData }}" aria-haspopup="true">
									<a href="{{ Route('book-quantity') }}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">Book Quantity</span>
									</a>
								</li>
								@endif
								@if(Route::currentRouteName() == 'book-page-count')         
								@php $activePageCountData = "m-menu__item--active" @endphp
								@else
								@php $activePageCountData = "" @endphp
								@endif
								@if(in_array('book-page-count',$accessData))
								<li class="m-menu__item {{ $activePageCountData }}" aria-haspopup="true">
									<a href="{{ Route('book-page-count') }}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">Page Count</span>
									</a>
								</li>
								@endif
								@if(Route::currentRouteName() == 'quote-durations')         
								@php $activeSavedQuoteDurationData = "m-menu__item--active" @endphp
								@else
								@php $activeSavedQuoteDurationData = "" @endphp
								@endif
								@if(in_array('quote-durations',$accessData))
								<li class="m-menu__item  {{ $activeSavedQuoteDurationData }}" aria-haspopup="true">
									<a href="{{ Route('quote-durations') }}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">Saved Quote Duration</span>
									</a>
								</li>
								@endif
								@if(Route::currentRouteName() == 'partial-payment-percentage')         
								@php $activePartialPaymentPercentageData = "m-menu__item--active" @endphp
								@else
								@php $activePartialPaymentPercentageData = "" @endphp
								@endif
								@if(in_array('partial-payment-percentage',$accessData))
								<li class="m-menu__item  {{ $activePartialPaymentPercentageData }}" aria-haspopup="true">
									<a href="{{ Route('partial-payment-percentage') }}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">Partial Payment</span>
									</a>
								</li>
								@endif
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<!-- END: Aside Menu -->
		</div>
		<!-- END: Left Aside -->  
		@yield('content')
	</div>
	<!-- begin::Footer -->
	<footer class="m-grid__item   m-footer ">
		<div class="m-container m-container--fluid m-container--full-height m-page__container">
			<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
				<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
					<span class="m-footer__copyright">
						2018 &copy; Digital Book All rights reserved.
					</span>
				</div>
			</div>
		</div>
	</footer>
	<!-- end::Footer -->    
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
	<i class="la la-arrow-up"></i>
</div>
<form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
	{{ csrf_field() }}
</form>
<!-- end::Scroll Top --> 
<!--begin::Global Theme Bundle -->
<script src="{{ URL::asset('assets/js/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<!--begin::Page Vendors -->
<script src="{{ URL::asset('assets/plugin/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/css/custom/components/base/sweetalert2.js') }}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Scripts -->
<script src="{{ URL::asset('assets/js/dashboard.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/custom.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/plugin/select/form-select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/bootstrap-datepickernew.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<!--end::Page Scripts -->
<script type="text/javascript">
	setTimeout(function() { $("#flash_message").hide('slow'); }, 5000);
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('form:first *:input[type!=hidden]:first').focus();
	});
</script>
@yield('content_js')
</body>
<!-- end::Body -->
</html>